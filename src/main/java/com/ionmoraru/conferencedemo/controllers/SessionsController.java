package com.ionmoraru.conferencedemo.controllers;

import com.ionmoraru.conferencedemo.models.Session;
import com.ionmoraru.conferencedemo.repositories.SessionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/sessions")
public class SessionsController {
    @Autowired
    private SessionRepository sessionRepository;

    @GetMapping
    public List<Session> list() {
        return sessionRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public Optional<Session> one(@PathVariable Long id) {
        return sessionRepository.findById(id);
    }

    @PostMapping
    public Session create(@RequestBody final Session session) {
        return sessionRepository.saveAndFlush(session);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    // need to cjeck for children records before deleteing.
    public void delete(@PathVariable Long id) {
        sessionRepository.deleteById(id);
    }

//    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
//    public Session update(@PathVariable Long id, @RequestBody Session session) {
//        // check if all attributes are passed in
//        Optional<Session> existingSession = sessionRepository.findById(id);
//
//        BeanUtils.copyProperties(session, existingSession, "session_id");
//        return sessionRepository.saveAndFlush(existingSession);
//
//
//    }


}
