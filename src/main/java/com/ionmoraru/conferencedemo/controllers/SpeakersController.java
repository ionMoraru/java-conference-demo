package com.ionmoraru.conferencedemo.controllers;

import com.ionmoraru.conferencedemo.models.Session;
import com.ionmoraru.conferencedemo.models.Speaker;
import com.ionmoraru.conferencedemo.repositories.SpeakerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/speakers")
public class SpeakersController {
    @Autowired
    private SpeakerRepository speakerRepository;

    @GetMapping
    public List<Speaker> list() {
        return speakerRepository.findAll();
    }

    @GetMapping
    @RequestMapping("{id}")
    public Optional<Speaker> get(@PathVariable Long id) {
        return speakerRepository.findById(id);
    }

    @PostMapping
    public Speaker create(@RequestBody final Speaker speaker) {
        return speakerRepository.saveAndFlush(speaker);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    // need to cjeck for children records before deleteing.
    public void delete(@PathVariable Long id) {
        speakerRepository.deleteById(id);
    }
//
//    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
//    public Speaker update(@PathVariable Long id, @RequestBody Speaker speaker) {
//        // check if all attributes are passed in
//        Optional<Speaker> existingSpeaker = speakerRepository.findById(id);
//
//        BeanUtils.copyProperties(speaker, existingSpeaker, "speaker_id");
//        return speakerRepository.saveAndFlush(existingSpeaker);
//
//
//    }
}
