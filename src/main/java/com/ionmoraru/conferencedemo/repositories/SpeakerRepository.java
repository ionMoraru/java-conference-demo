package com.ionmoraru.conferencedemo.repositories;

import com.ionmoraru.conferencedemo.models.Speaker;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpeakerRepository extends JpaRepository<Speaker, Long> {
}
